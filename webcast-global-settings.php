<?php
/*
Plugin Name: Webcast Global Settings
Version: 1.0
Description: Global options for the current webcast to run. 
Author: Elvis Morales
Author URI: http://elvismdev.io
Plugin URI: https://bitbucket.org/grantcardone/webcast-global-settings
Text Domain: webcast-global-settings
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

add_action( 'admin_menu', 'wgs_add_admin_menu' );
add_action( 'admin_init', 'wgs_settings_init' );


function wgs_add_admin_menu(  ) { 

	add_menu_page( 'Webcast Global Settings', 'Webcast Global Settings', 'manage_options', 'webcast_global_settings', 'wgs_options_page' );

}


function wgs_settings_init(  ) { 

	register_setting( 'pluginPage', 'wgs_settings' );

	add_settings_section(
		'wgs_pluginPage_section', 
		__( '', 'webcast-global-settings' ), 
		'wgs_settings_section_callback', 
		'pluginPage'
		);

	add_settings_field( 
		'wgs_homepage_banner_login_popup', 
		__( 'Login popup on banner', 'webcast-global-settings' ), 
		'wgs_homepage_banner_login_popup_render', 
		'pluginPage', 
		'wgs_pluginPage_section' 
		);

	add_settings_field( 
		'wgs_webcast_landing_page', 
		__( 'Webcast landing page', 'webcast-global-settings' ), 
		'wgs_webcast_landing_page_render', 
		'pluginPage', 
		'wgs_pluginPage_section' 
		);

	add_settings_field( 
		'wgs_webcast_page_id', 
		__( 'Webcast page ID', 'webcast-global-settings' ), 
		'wgs_webcast_page_id_render', 
		'pluginPage', 
		'wgs_pluginPage_section' 
		);



	// Per hour webcasts page IDs

	add_settings_field( 
		'wgs_8am_webcast_page_id', 
		__( '8:00am - Webcast page ID', 'webcast-global-settings' ), 
		'wgs_8am_webcast_page_id_render', 
		'pluginPage', 
		'wgs_pluginPage_section' 
		);

	add_settings_field( 
		'wgs_1pm_webcast_page_id', 
		__( '1:00pm - Webcast page ID', 'webcast-global-settings' ), 
		'wgs_1pm_webcast_page_id_render', 
		'pluginPage', 
		'wgs_pluginPage_section' 
		);

	add_settings_field( 
		'wgs_6pm_webcast_page_id', 
		__( '6:00pm - Webcast page ID', 'webcast-global-settings' ), 
		'wgs_6pm_webcast_page_id_render', 
		'pluginPage', 
		'wgs_pluginPage_section' 
		);


}


function wgs_homepage_banner_login_popup_render(  ) { 

	$options = get_option( 'wgs_settings' );
	?>
	<input type='checkbox' name='wgs_settings[wgs_homepage_banner_login_popup]' <?php checked( $options['wgs_homepage_banner_login_popup'], 1 ); ?> value='1'>
	<?php

}


function wgs_webcast_landing_page_render(  ) { 

	$options = get_option( 'wgs_settings' );
	?>
	<input type='text' name='wgs_settings[wgs_webcast_landing_page]' value='<?php echo $options['wgs_webcast_landing_page']; ?>'>
	<?php

}


function wgs_webcast_page_id_render(  ) { 

	$options = get_option( 'wgs_settings' );
	?>
	<input type='text' name='wgs_settings[wgs_webcast_page_id]' value='<?php echo $options['wgs_webcast_page_id']; ?>'>
	<?php

}



// Per hour webcasts page IDs

function wgs_8am_webcast_page_id_render(  ) { 

	$options = get_option( 'wgs_settings' );
	?>
	<input type='text' name='wgs_settings[wgs_8am_webcast_page_id]' value='<?php echo $options['wgs_8am_webcast_page_id']; ?>'>
	<?php

}

function wgs_1pm_webcast_page_id_render(  ) { 

	$options = get_option( 'wgs_settings' );
	?>
	<input type='text' name='wgs_settings[wgs_1pm_webcast_page_id]' value='<?php echo $options['wgs_1pm_webcast_page_id']; ?>'>
	<?php

}

function wgs_6pm_webcast_page_id_render(  ) { 

	$options = get_option( 'wgs_settings' );
	?>
	<input type='text' name='wgs_settings[wgs_6pm_webcast_page_id]' value='<?php echo $options['wgs_6pm_webcast_page_id']; ?>'>
	<?php

}



function wgs_settings_section_callback(  ) { 

	echo __( 'Define below general settings for the actual running webcast. <br>
		<em>Clear cache after changing this settings.</em><br><br>
		<em>Leave empty the per/hour "Webcast page ID" fields in order to use one webcast page only and redirect the users only to this single one.</em>', 'webcast-global-settings' );

}


function wgs_options_page(  ) { 

	?>
	<form action='options.php' method='post'>
		
		<h2>Webcast Global Settings</h2>
		
		<?php
		settings_fields( 'pluginPage' );
		do_settings_sections( 'pluginPage' );
		submit_button();
		?>
		
	</form>
	<?php

}
